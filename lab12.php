<style>
    <?php include 'lab08.css';
    ?>
</style>
<?php
$khoa = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);
session_start();
if ($_SESSION['listName'] != null) {
    if ($_SESSION['listName'][count($_SESSION['listName']) - 1] !=  $_SESSION['name']) {
        $_SESSION['listName'][] = $_SESSION['name'];
        $_SESSION['listFaculty'][] = $_SESSION['faculty'];
    }
} else {
    $_SESSION['listName'] = [];
    $_SESSION['listFaculty'] = [];
}


?>
<script>
    window.onload = function() {

        var faculty = localStorage.getItem("faculty");
        var keyWord = localStorage.getItem("keyWord");
        const collectElement = document.getElementsByClassName("input1");
        collectElement[0].value = faculty;
        collectElement[1].value = keyWord;
    }

    function deleteIndexValue(a) {
        const collectElement = document.getElementsByClassName(a);
        for (let i = 0; i < collectElement.length; i++) {
            collectElement[i].value = "";
        }
        localStorage.removeItem("faculty");
        localStorage.removeItem("keyWord");
    }

    function saveData(a) {
        const collectElement = document.getElementsByClassName(a);
        localStorage.setItem("faculty", collectElement[0].value);
        localStorage.setItem("keyWord", collectElement[1].value);
        console.log(localStorage.getItem(faculty));
    }
</script>
<html>

<head>
    <title>Trang đăng nhập</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</head>

<body>
    <div class="mx-auto mt-5 border d-flex flex-column" id="container" style="max-width: 700px !important  ">
        <div class="my-3 d-flex flex-column mx-auto">
            <form>
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Khoa<span> *</span></label>
                    <select class="input1" name="department" style="width: 189px; border: 1px solid #2980d7">
                        <option></option>
                        <?php foreach ($khoa as $i =>
                            $khoa) : ?>
                            <option><?php echo $khoa ?></option>
                        <?php endforeach; ?>
                    </select>
                    <br /><br />
                </div>
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Từ khóa</label>
                    <input class="h-100 input1" type="text" style="border: 1px solid #2980d7" name="key">
                </div>
                <div class="d-flex">
                    <button onclick="deleteIndexValue('input1')" class="btn1 mx-auto btn btn-primary btn-submit mt-3">Xóa</button>
                    <button onclick="saveData('input1')" class="mx-auto btn btn-primary btn-submit mt-3">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="d-flex justify-content-between mx-2">
            <span style="font-weight: 500">Số sinh viên tìm thấy : XXX</span>
            <button onclick="window.location='labform08.php'" class="btn btn-primary btn-add">Thêm</button>
        </div>
        <div class="mt-3">
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <?php for ($x = 0; $x < count($_SESSION['listName']); $x++) { ?>
                    <tr>
                        <td>

                            <?php echo $x + 1 ?>

                        </td>
                        <td>
                            <?php echo $_SESSION['listName'][$x] ?>
                        </td>
                        <td> <?php echo $_SESSION['listFaculty'][$x] ?> </td>
                        <td>
                            <div>
                                <button class="btn btn-action">Xóa</button>
                                <button class="btn btn-action">Sửa</button>
                            </div>
                        </td>
                    </tr>
                    <tr>

                    <?php } ?>
            </table>
        </div>
    </div>
</body>

</html>













<!-- <td>2</td>
<td>Lò Thị B</td>
<td>Khoa học máy tính</td>
<td>
    <div>
        <button class="btn btn-action">Xóa</button>
        <button class="btn btn-action">Sửa</button>
    </div>
</td>
</tr>
<tr>
    <td>3</td>
    <td>Lò Văn A</td>
    <td>Khoa học vật liệu</td>
    <td>
        <div>
            <button class="btn btn-action">Xóa</button>
            <button class="btn btn-action">Sửa</button>
        </div>
    </td>
</tr>
<tr>
    <td>4</td>
    <td>Đào Thị Minh Đức</td>
    <td>Khoa học vật liệu</td>
    <td>
        <div>
            <button class="btn btn-action">Xóa</button>
            <button class="btn btn-action">Sửa</button>
        </div>
    </td>
</tr> -->
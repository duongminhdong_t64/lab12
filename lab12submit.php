<?php

session_start();
function nextpage()
{
    header('location:lab08.php');
}
if (isset($_GET['ok'])) {

    nextpage();
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="lab08submit.css">
</head>

<body>
    <div id='backDiv'>
        <form method="POST" style="position: center !important;
    width:80%;
    margin-left: 10%;
    margin-right: 10%;">
            <div id="infoDiv" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên </label>
                <div class="info">
                    <?php echo $_SESSION['name'] ?>
                </div>
            </div>

            <div id="infoDiv">
                <label id="label" class="h-100" for="gender"> Giới tính </label>
                <div class="info">
                    <?php echo $_SESSION['gender'] ?>
                </div>
            </div>

            <div id="infoDiv">
                <label id="label" class="h-100">Phân khoa </label>
                <div class="info">
                    <?php echo $_SESSION['faculty'] ?>
                </div>
            </div>

            <div id="infoDiv">
                <label id="label" class="h-100" for="birthday">Ngày sinh </label>
                <div class="info">
                    <?php echo $_SESSION['date'] ?>
                </div>
            </div>

            <div id="infoDiv">
                <label id="label" class="h-100" for="address">Địa Chỉ </label>
                <div class="info">
                    <?php echo $_SESSION['address'] ?>
                </div>
            </div>

            <div id="infoDiv">
                <label id="label" class="h-100">Hình ảnh </label>
                <img id="imgShow" src="<?php if ($_SESSION['image'] != 'upload/') {
                                            echo $_SESSION['image'];
                                        } ?>" class="thump-image" style="width: 100px; height: 130px;  margin-left:10%; margin-bottom:3%;margin-top:1%;" alt="Avatar image">
                <!-- <span class="prfil-img"><img src="<?= $_SESSION['image']; ?>" alt=""> </span> -->
            </div>

            <div id="btnDiv" style="margin-top: 30%;">
                <form action="lab08.php" method="get">
                    <button name="ok" value="ok" class="btn btn-success" id="submitId"> Xác nhận </button>
                </form>

            </div>
        </form>
    </div>

</body>

</html>
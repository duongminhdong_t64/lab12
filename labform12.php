<?php
$gioiTinh = array(
    0 => 'Nam',
    1 => 'Nữ'
);
$khoa = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="labform12.css">
</head>

<body>

    <div id='backDiv'>
        <form method="POST" style="position: center !important;
            width:80%;
            margin-left: 10%;
            margin-right: 10%;" enctype="multipart/form-data">
            <?php
            $name = "none";
            $date = "none";
            $gender = "none";
            $faculty = "none";
            $address = "none";
            $check_image = true;
            // $id  = 0;
            function emptyInputFeild($input)
            {
                return $input == '';
            }

            function emptyImage($input)
            {
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (emptyInputFeild($_POST["name"])) {
                    echo "<div style='color: red;'>Hãy nhập họ tên của bạn . </div>";
                }
                if (!isset($_POST['gender'])) {

                    echo "<div style='color: red;'>Hãy chọn giới tính của bạn .</div>"; // echo the choice 
                }
                if (emptyInputFeild($_POST["faculty"])) {
                    echo "<div style='color: red;'>Hãy chọn phân khoa .</div>";
                }
                if (emptyInputFeild($_POST["date"])) {
                    echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
                } elseif (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST["date"])) {
                    echo "<div style='color: red;'> Hãy nhập ngày sinh đúng định dạng </div>";
                }
                // if (emptyInputFeild($_POST["address"])) {
                //     echo "<div style='color: red;'>Hãy nhập địa chỉ của bạn .</div>";
                // }

                if ($_FILES['image'] == null) {
                    echo "<div style='color: red;'>Hãy chèn hình ảnh của bạn .</div>";
                }
                if (isset($_FILES['image'])) {
                    $mimetype = mime_content_type($_FILES['image']['tmp_name']);
                    if (in_array($mimetype, array('image/jpeg', 'image/gif', 'image/png')) == false) {
                        echo "<div style='color: red;'>File bạn chọn không phải hình ảnh vui lòng chọn lại .</div>";
                        $check_image = false;
                    }
                }
                if (
                    !emptyInputFeild($_POST["name"])
                    && !emptyInputFeild($_POST["gender"])
                    && !emptyInputFeild($_POST["faculty"])
                    && !emptyInputFeild($_POST["date"])
                    && $_FILES['image'] != null
                    && $check_image
                ) {
                    $image = $_FILES['image'] ?? null;
                    // $imagePath = '';

                    $date = date_create($_POST['date']);
                    $result = date_format($date, "YmdHis");
                    $date = date_format($date, "d-m-Y H:i:s");

                    if (!is_dir('upload')) {
                        mkdir('upload');
                    }

                    if ($image) {
                        mkdir(dirname($_POST['image']));

                        $filename = pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                        $extension  = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION); // jpg
                        $basename   = "{$filename}_" . $result . "." . $extension; // 5dab1961e93a7_1571494241.jpg

                        $source       = $_FILES["image"]["tmp_name"];
                        $destination  = "upload/{$basename}";

                        /* move the file */
                        move_uploaded_file($source, $destination);
                    }



                    session_start();
                    $_SESSION['name'] = $_POST['name'];
                    $_SESSION['gender'] = $_POST['gender'];
                    $_SESSION['faculty'] = $_POST['faculty'];
                    $_SESSION['date'] = $date;
                    $_SESSION['address'] = $_POST['address'];
                    $_SESSION['image'] = $destination;
                    header('location:submit_succed.php');
                }
                importdatabase($khoa);
                $id = $id + 1;
            }
            function importdatabase($arr)
            {
                $mysqli = new mysqli("localhost", "root", "", "php_validate");

                try {
                    $checkexits = mysqli_query($mysqli, "DESCRIBE `student`");
                } catch (\Throwable $th) {
                    $checkexits = FALSE;
                }
                $gender = $_SESSION['gender'] == "Nam" ? 1 : 0;;
                $faculySign = array_search($_SESSION['faculty'], $arr) ?? 'none';
                $datetime = strtotime($_SESSION['date']);
                $newformat  = date('Y-m-d H:i:s', $datetime);
                // echo ysqli_query($mysqli, "DESCRIBE `student`") ;
                if ($checkexits === FALSE) {
                    $sql = "CREATE TABLE `student` (
                        `id` int(11) NOT NULL AUTO_INCREMENT ,
                        `name` varchar(250) NOT NULL,
                        `gender` int(1) NOT NULL,
                        `faculty` char(3) NOT NULL,
                        `birthday` datetime NOT NULL,
                        `address` varchar(250) DEFAULT NULL,
                        `avartar` text DEFAULT NULL,
                        PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                    if ($mysqli->query($sql) === TRUE) {
                        echo "New record created successfully";
                        $sql = "INSERT INTO student (name,gender,faculty,birthday,address,avartar) VALUES ('{$_SESSION['name']}', $gender, '{$faculySign}', '{$newformat}','{$_SESSION['address']}','{$_SESSION['image']}')";
                        if ($mysqli->query($sql) === TRUE) {
                            echo "Record insert successfully";
                        } else {
                            echo "Error: " . $sql . "<br>" . $mysqli->error;
                        }
                    } else {
                        echo "Error: " . $sql . "<br>" . $mysqli->error;
                    }
                } else {

                    $sql = "INSERT INTO student (name,gender,faculty,birthday,address,avartar) VALUES ('{$_SESSION['name']}', $gender, '{$faculySign}', '{$newformat}','{$_SESSION['address']}','{$_SESSION['image']}')";
                    if ($mysqli->query($sql) === TRUE) {
                        echo "Record insert successfully";
                    } else {
                        echo "Error: " . $sql . "<br>" . $mysqli->error;
                    }
                }


                $mysqli->close();
                echo '<div class="success-response sql-import-response">SQL file imported successfully</div>';
            }
            ?>
            <div id="nameDiv" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên </label>
                <input id="input" class="h-100" type="text" name="name">
                <!-- required="" placeholder="Dương Minh Đông" oninvalid="this.setCustomValidity('Hãy nhập tên của bạn')" oninput="setCustomValidity('')" -->
            </div>
            <div id="infoDiv">
                <label id="label" class="h-100"> Giới tính </label>
                <div id="radioBtn">
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <input id="radioBtn" type="radio" name='gender' class="gender" value='<?php echo $gioiTinh[$i] ?>'>
                        <?php echo $gioiTinh[$i] ?>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div id="infoDiv">
                <label id="label" class="h-100">Phân khoa </label>
                <select id="input" class="h-100" name="faculty">
                    <!-- required="" oninvalid="this.setCustomValidity('Hãy chọn phân khoa của bạn')" oninput="setCustomValidity('')" -->
                    <?php foreach ($khoa  as $i) : ?>
                        <option><?php echo $i ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div id="dateDiv">
                <label id="label" class="h-100" for="birthday">Ngày sinh </label>
                <!-- <input id="input" class="h-100" type="text" name="date" placeholder="dd/mm/yyyy"> -->
                <td height='40px'>
                    <input type='date' id="input" class="h-100" name="date" data-date="" data-date-format="DD MMMM YYYY" style='line-height: 32px; border-color:#ADD8E6'>
                </td>
                <!-- required="" oninvalid="this.setCustomValidity('Hãy nhập ngày sinh đúng định dạng')" oninput="setCustomValidity('')" -->
                <script>
                    $(document).ready(function() {
                        $("#input").datepicker({
                            format: 'dd-mm-yyyy' //can also use format: 'dd-mm-yyyy'     
                        });
                    });
                </script>

            </div>
            <div id="addressDiv">
                <label id="label" class="h-100" for="address">Địa Chỉ </label>
                <input id="input" class="h-100" type="text" name="address">
            </div>
            <div id="imageDiv">
                <label id="label" class="h-100" for="image">Hình ảnh </label>
                <input id="input" type="file" accept="image/*" onchange="loadFile(event)" name="image">
                <img id="outputimg" style="width: 100px; height: 130px;  margin-left:40%; margin-bottom:3%;margin-top:1%;" />
                <script>
                    var loadFile = function(event) {
                        var reader = new FileReader();
                        reader.onload = function() {
                            var output = document.getElementById('outputimg');
                            output.src = reader.result;
                        };
                        reader.readAsDataURL(event.target.files[0]);
                    };
                </script>
            </div>
            <div id="btnDiv">
                <button class="btn btn-success" id="submitId">Đăng ký</button>
            </div>

        </form>
    </div>

</body>

</html>